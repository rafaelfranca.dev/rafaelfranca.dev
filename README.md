# Olá pessoal

Eu soum um desenvolvedor fullstack formado em análise e desenvolvimento de sistemas, atualmente estou trabalhando no setor de tecnologia. 😍

### O que eu faço

Trabalho principalmente com o Java e Angular, adoro criar aplicações web, na minha opinião é a melhor combinação de programação lógica e
(as vezes 👀) lindos designs.

Minha especialidade é controlar fluxos e manipular dados, gosto de ter controle da aplicação mesmo quando em tempo de execução.

## Skills 📜

### Desenvolvimento de Aplicações

- PHP
- Java
- Angular
- Next.js
- JavaScript
- TypeScript

### Conhecimento Técnico

- Git
- Docker
- Servlets
- Struts/tiles
- Spring framework(Security, Batch, Web, Data)
- Quartz
- AWS
- CI/CD
- Sonar
- Lambda
- PostgreSql
- dynamoDB
- HTML, CSS ,SCSS
- Node.js
- Testes Unitários(Junit, Jest, jasmine)

### Certificados

- Planejamento Estratégico de TI no setor Público.
- Banco de Dados e novas tendências.
- Cloud Computing e Internet das Coisas.
- Virtualização de Sistemas Operacionais.
- Testes Unitários em Java Utilizando o JUnit

### Linguagens 🌐

| linguagem | Nível            |
| --------- | ---------------- |
| Inglês    | Intermediário    |
| Português | Linguagem Nativa |
